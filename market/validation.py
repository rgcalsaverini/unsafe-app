"""
Deals with data validation for the API
"""
import datetime
import json
from copy import deepcopy

from .back_office import confirm_card


class Validator(object):
    """
        Offers validation functionality to posted data.

        - user_data validates data related to user account
        - cvv validates data related to card verification number
    """
    def __init__(self, data, **extra_args):
        """
        :param data: the data received from the user
        :param extra_args: arbitrary extra data
        """
        self.extra_args = extra_args
        self.data = deepcopy(data)

    def user_data(self):
        fields = ['name', 'email', 'card', 'password']
        return self._validate(fields)

    def cvv(self):
        fields = ['cvv']
        return self._validate(fields)

    def _validate(self, fields):
        """
        Generic validation method, receives list of validation rules and apply
        them in order. Break and report failure if any of them fails.

        Each validation rule is tied to a validation method, that aside from
        validating, can also change the data, that is then returned to be
        consumed by the routes
        """
        for item in fields:
            method = '_validate_%s' % item
            if not method:
                return False, None
            response = getattr(self, method)()
            if isinstance(response, (tuple, list)) and not response[0]:
                return False, response[1]
        return True, self.data

    def _validate_name(self):
        name = self.data.get('name', '')
        if not name:
            return False, 'Invalid name'
        if len(name) < 5:
            return False, 'Name too short'
        return True

    def _validate_email(self):
        email = self.data.get('email', '')
        if not email or email.count('@') != 1 or email.count('.') < 1:
            return False, 'Invalid email'
        return True

    def _validate_card(self):
        card_expiration = self.data.get('card_expiration', '')
        card_number = self.data.get('card_number', '')
        date_bits = card_expiration.split('/')
        user = self.extra_args.get('user', None)

        if user:
            masked_card = ('*' * 12) + str(user.card_number)[12:]
            if card_number == masked_card:
                card_number = user.card_number

        if not card_expiration or '/' not in card_expiration:
            return False, 'Invalid expire date'

        if len(date_bits) != 2:
            return False, 'Invalid expire date'

        month = int(date_bits[0])
        year = int(date_bits[1])

        response = json.loads(confirm_card(card_number, month, year))
        if not response['success']:
            return False, response['error']

        self.data['card_number'] = card_number
        self.data['card_expiration'] = datetime.date(year, month, 1)

        return True

    def _validate_password(self):
        pwd = self.data.get('password', '')
        pwd_repeat = self.data.get('pwd_repeat', '')

        if self.extra_args.get('first_time', False) or pwd:
            if not pwd or len(pwd) < 6:
                return False, 'Invalid password'
            if pwd != pwd_repeat:
                return False, 'Password does not match'
        else:
            pwd = self.extra_args['user']['password']
        if 'pwd_repeat' in self.data.keys():
            del self.data['pwd_repeat']
        self.data['password'] = pwd

    def _validate_cvv(self):
        cvv = self.data.get('cvv', None)
        if not cvv:
            return False, 'Invalid CVV'
        cvv = int(cvv)
        if int(cvv) < 100 or int(cvv) > 999:
            return False, 'Invalid CVV'
        return True
