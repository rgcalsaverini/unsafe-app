from mongoengine import *

from market.utils import md5_of


class Product(Document):
    id = StringField(primary_key=True, required=True)
    name = StringField(required=True)
    selling_points = ListField(StringField())
    price = FloatField(required=True)
    comments = ListField(StringField())
    paused = BooleanField(required=True, default=False)

    def to_dict(self):
        return {
            'id': self.id,
            'sku': self.id,
            'name': self.name,
            'selling_points': self.selling_points,
            'price': self.price,
            'comments': self.comments,
            'paused': self.paused or False,
        }

    def add_comments(self, *new_comments):
        self.comments = (self.comments + list(new_comments))[-20:]
        self.save()


class User(Document):
    id = IntField(primary_key=True, required=True)
    name = StringField(required=True)
    email = EmailField(required=True, unique=True)
    card_number = IntField(required=True,
                           min_value=1000000000000000,
                           max_value=9999999999999999)
    card_expiration = DateField(required=True)
    admin = BooleanField(required=True, default=False)
    password = StringField(required=True)

    def to_dict(self):

        return {
            'id': self.id,
            'name': self.name,
            'email': self.email,
            'card_number': str(self.card_number),
            'card_expiration_m': '%0.2d' % self.card_expiration.month,
            'card_expiration_y': '%0.2d' % self.card_expiration.year,
            'admin': self.admin,
            'password': self.password,
        }

    @staticmethod
    def add(password, **data):
        user = User.objects.order_by('-id').first()
        if not user:
            next_id = 1
        else:
            next_id = user.id + 1
        return User(
            password=md5_of(password),
            id=next_id,
            **data,
        ).save()
