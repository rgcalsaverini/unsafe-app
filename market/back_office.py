"""
This module deals with the integration of the Market API and the back-office
systems, such as the tax application and the digital point of sale.
"""
import json
import os
import subprocess

from .utils import dir_path

root_dir = os.path.abspath(
    os.path.join(dir_path(__file__).split('/market/')[0], '/market')
)


def load_json(filename):
    """ Return the parsed JSON or None """
    try:
        with open(filename, 'r') as json_file:
            return json.loads(json_file.read())
    except (OSError, ValueError):
        return None


def register_sale(data):
    """ Register a sale on the back-office application """
    config = load_json(os.path.join(root_dir, 'configs.json'))
    command = config.get('back_office_cmd', None)
    if not command:
        return None
    pipe = subprocess.Popen(command.split(),
                            stdout=subprocess.PIPE,
                            stdin=subprocess.PIPE,
                            stderr=subprocess.STDOUT)
    stdout = pipe.communicate(input=json.dumps(data).encode('utf-8'))[0]
    return stdout.decode('utf-8')


def confirm_card(card, y, m):
    """ Confirm that the information provided regarding a card is valid  """
    config = load_json(os.path.join(root_dir, 'configs.json'))
    command = config.get('back_office_cmd', None)
    if not command:
        return None
    full_cmd = '{} validate {} {} {}'.format(command, card, y, m)
    process = subprocess.Popen(full_cmd, stdout=subprocess.PIPE, shell=True)
    stdout = process.communicate()[0].strip()
    return stdout.decode('utf-8')


def calculate_taxes(value):
    """ Calculate taxes over a product's value """
    config = load_json(os.path.join(root_dir, 'configs.json'))
    command = config.get('taxes_cmd', None)
    if not command:
        return None
    full_cmd = command + ' --value=%s' % value
    process = subprocess.Popen(full_cmd, stdout=subprocess.PIPE, shell=True)
    stdout = process.communicate()[0].strip()
    return stdout.decode('utf-8')
