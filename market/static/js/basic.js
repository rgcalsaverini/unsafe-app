if(document.location.href.indexOf('error') > -1){
  $('.error').fadeTo("fast", 1);
  const error = decodeURI(document.location.href.split('error=')[1]);
  $('.error').html(decodeURIComponent(error.replace(/\+/g, ' ')));
}

function send_task(form_id, extras={}) {
  const inputs = $('#' + form_id).find('select, input');
  const data = {id: form_id, ...extras};
  for(let i = 0 ; i < inputs.length ; i++) {
    data[inputs[i].name] = inputs[i].value;
  }
  $.ajax({
    type: "POST",
    url: '/add-to-queue',
    data: JSON.stringify(data),
    complete: function() { location.reload(); },
    dataType: 'json'
  });
}
