"""
This module collects other miscellaneous routes not directly related to
 user management and the purchase process, such as:

- root            GET         /
- show_login      GET         /login
- perform_login   POST        /login
- perform_logout  GET         /logout
- get_img         GET         /img

"""

import os

from flask import redirect, request, abort, send_file

from market.models import Product, User
from market.utils import md5_of, dir_path, resize_image
from .utils import clear_session, set_session
from .utils import templated, is_logged_in, get_data, redirect_error


def create_other_routes(bp):
    @bp.route('/', methods=['GET'])
    @templated('main.html')
    def root():
        """ App's home """
        products = Product.objects.all()
        return dict(products=products)

    @bp.route('/login', methods=['GET'])
    @templated('login.html')
    def show_login():
        """ Login page. Redirects to home if already logged in """
        if is_logged_in():
            return redirect('/')

    @bp.route('/login', methods=['POST'])
    def perform_login():
        """
        Logs in a user, than, if the user is comming from another page,
        redirects him to it. Otherwise just redirect to home
        """
        data = get_data()
        user = User.objects.filter(email=data.get('user', None)).first()
        if not user:
            return redirect_error('/login', 'User not found')
        if user.password != md5_of(data.get('pwd', '')):
            return redirect_error('/login', 'Invalid password')

        return set_session(user)

    @bp.route('/logout', methods=['GET'])
    def perform_logout():
        """ Logs user out """
        clear_session()
        return redirect('/')

    @bp.route('/img', methods=['GET'])
    def get_img():
        """
        Return an image resource with the possibility of rescaling it.
        If an image with the exact dimensions was already created, serve it
        from disk.
        If the argument 'width' is not provided, no rescaling will take place.
        """
        img = request.args.get('file', None)
        width = get_width()
        img_path = os.path.join(dir_path(__file__), '../static', img)

        if not os.path.isfile(img_path):
            abort(404)
        if width is not None:  # resizing required
            img_path = resize_image(img_path, width)

        return send_file(img_path)


def get_width():
    """ Retrieves a valid width from query args """
    width_str = request.args.get('width', None)
    try:
        width = int(width_str)
    except (ValueError, TypeError):
        return None
    return width
