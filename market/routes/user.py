"""
This module groups routes related to user management such as:

- show_account     GET        /account
- show_register    GET        /register
- update_account   POST       /account
- register         GET        /register

"""

from flask import redirect, session
from mongoengine.errors import ValidationError

from market.models import User
from market.validation import Validator
from .utils import redirect_error, set_session
from .utils import templated, is_logged_in, must_login, get_data


def create_user_routes(bp):
    @bp.route('/account', methods=['GET'])
    @must_login
    @templated('acc.html')
    def show_account():
        """ show the account page """
        return {}

    @bp.route('/register', methods=['GET'])
    @templated('acc.html')
    def show_register():
        """ show the register page """
        return {}

    @bp.route('/account', methods=['POST'])
    @must_login
    def update_account():
        """ update changes to the user account """
        data = get_data()
        user = User.objects.filter(id=session['user']['id']).first()

        if not user:
            return redirect_error('/account', 'Invalid user')

        validator = Validator(data, user=user)
        valid, result = validator.user_data()

        if not valid:
            return redirect_error('/account', result)

        for field, value in result.items():
            setattr(user, field, value)
        try:
            user.save()
        except ValidationError:
            return redirect_error('/register')
        session['user'] = user.to_dict()
        return redirect('/')

    @bp.route('/register', methods=['POST'])
    def register():
        """ Register a new user account """
        if is_logged_in():
            return redirect('/')
        data = get_data()
        validator = Validator(data, first_time=True)
        valid, result = validator.user_data()
        if not valid:
            return redirect_error('/register', result)
        password = result.pop('password')
        try:
            user = User.add(password, **result)
        except ValidationError:
            return redirect_error('/register')
        return set_session(user)
