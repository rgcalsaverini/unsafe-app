"""
Routes specifically related with the admin flow of data.

- admin_panel      GET         /admin
- execute_queue    GET         /execute-queue
- clear_queue      GET         /clear-queue
- add_to_queue     POST        /add-to-queue

"""
from flask import session, redirect

from market.models import Product, User, md5_of
from .utils import must_be_admin, templated, check_queue, get_data


def create_admin_routes(bp):
    @bp.route('/admin', methods=['GET'])
    @must_be_admin
    @templated('admin.html')
    def admin_panel():
        """ Shows the admin panel """
        products = Product.objects.all()
        users = User.objects.all()
        return {
            'products': products,
            'users': users,
            'queue': session.get('task_queue', []),
        }

    @bp.route('/execute-queue', methods=['GET'])
    @check_queue
    @must_be_admin
    def execute_queue():
        """ Execute the tasks queue on the admin's queue """
        queue = session['task_queue']
        for item in queue:
            task_handler = tasks.get(item['id'], None)
            if task_handler:
                task_handler(item)
        session['task_queue'] = []
        return redirect('/admin')

    @bp.route('/clear-queue', methods=['GET'])
    @check_queue
    @must_be_admin
    def clear_queue():
        """ Clear the admin queue """
        session['task_queue'] = []
        return redirect('/admin')

    @bp.route('/add-to-queue', methods=['POST'])
    @check_queue
    @must_be_admin
    def add_to_queue():
        """ add a task to the admin queue """
        data = get_data()
        session['task_queue'].append(data)
        return ''


def pause_unpause(item):
    product_id = item.get('product_id')
    pause = item.get('pause', False)
    product = Product.objects.filter(id=product_id).first()
    if not product:
        return
    product.paused = pause
    product.save()


def reset_password(item):
    user_id = item.get('user_id')
    new_password = md5_of(item.get('new_password', ''))
    user = User.objects.filter(id=user_id).first()
    if not user:
        return
    user.password = new_password
    user.save()


def promote_demote(item):
    user_id = item.get('user_id')
    admin = item.get('admin', False)
    user = User.objects.filter(id=user_id).first()
    if not user:
        return
    user.admin = admin
    user.save()


tasks = {
    'pause_unpause': pause_unpause,
    'reset_password': reset_password,
    'promote_demote': promote_demote,
}
