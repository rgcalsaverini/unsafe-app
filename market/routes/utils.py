from functools import wraps
from urllib import parse as url_parse

from flask import request, session, render_template, abort
from werkzeug.utils import redirect
from werkzeug.wrappers import Response

from market.models import Product

empty_user = {}

task_whitelist = [
    'reset_password',
    'pause_unpause',
    'promote_demote',
]


def set_session(user):
    """
    Sets a session for the user passed, return a redirect to the next URL
    and clear it
    """
    session['user'] = user.to_dict()
    next_url = session.get('next', '/')
    session['next'] = None
    return redirect(next_url or '/')


def clear_session():
    """ Replaces the session with an non-logged one """
    session.clear()
    session['buy_id'] = None
    session['user'] = empty_user


def with_headers(headers):
    """ Create a Response object with some default headers """

    class CustomResponse(Response):
        def __init__(self, *args, **kwargs):
            super(CustomResponse, self).__init__(*args, **kwargs)
            self.headers.extend(headers)

    return CustomResponse


def redirect_error(location, error=None, arg=True):
    """
    Redirect the user to another page, while setting error headers and
    a safely encoded error message on the URL.
    """

    default_msg = 'Something went wrong'

    url_parts = list(url_parse.urlparse(location))
    query = dict(url_parse.parse_qsl(url_parts[4]))
    if error is None:
        error = default_msg

    if arg:
        query.update({'error': error})
    url_parts[4] = url_parse.urlencode(query)
    final_location = url_parse.urlunparse(url_parts)

    error_headers = {
        'X-Failed': 'YES',
        'X-Error-Message': error,
    }

    return redirect(final_location, Response=with_headers(error_headers))


def get_data():
    """
    Get posted data regardless of codification, first try json, than
    form encoded. That means that for the Market application, it is irrelevant
    if the data is posted as
        {"user": "admin", "password": "123" }
    or
        user=admin&password=123
    """
    data = request.get_json(force=True, silent=True)
    if not data:
        data = {k: (v[0] if isinstance(v, list) else v).strip()
                for k, v in request.form.items()}
    if not data:
        return {}
    return data


def is_logged_in():
    """ Check is the user is logged """
    user = session.get('user', False)
    if user and user != empty_user and user['email'] and user['name']:
        return True
    return False


def is_admin():
    """ Check if the user is an admin """
    if not is_logged_in():
        return False

    # For debugging only, remove latter!
    admin_header = request.headers.get('Admin', 'NO') == 'YES'

    admin_user = session['user'].get('admin') is True
    admin_task = session.get('task_queue', None) is not None

    if admin_user or admin_task or admin_header:
        return True
    return False


def check_queue(f):
    """ Checks if an admin's task queue is valid """
    @wraps(f)
    def decorated(*args, **kwargs):
        queue = [v for v in session.get('task_queue', []) if v]
        valid_queue = []
        for task in queue:
            tid = task.get('id', None)
            if tid not in task_whitelist:
                continue
            valid_queue.append(task)
        session['task_queue'] = valid_queue[:10]  # no more than 10 tasks
        return f(*args, **kwargs)

    return decorated


def must_login(f):
    """
    Decorate routes where the user must be logged to access, if the user
     is not logged redirects to login page.
     """

    @wraps(f)
    def decorated(*args, **kwargs):
        if not is_logged_in():
            return redirect_error('/login', 'Must login', arg=False)
        return f(*args, **kwargs)

    return decorated


def must_be_admin(f):
    """
    Decorate routes where the user must be a logged admin to access.
    """

    @wraps(f)
    def decorated(*args, **kwargs):
        if not is_admin():
            return abort(403)
        return f(*args, **kwargs)

    return decorated


def must_be_purchase(f):
    """
    Decorate routes where the user must be in the middle of a purchase.
    If the user is not, redirects to home.
    """

    @wraps(f)
    def decorated(*args, **kwargs):
        product_id = session['buy_id']
        product = Product.objects.filter(id=product_id, paused=False).first()
        if not product:
            return redirect_error('/', 'Not on a purchase', arg=False)
        return f(*args, product=product, **kwargs)

    return decorated


def templated(template):
    """
    WARNING: This decorator must be the bottom-most one

    Helper to add a template to a route. It receives the name of the template
    as the sole argument, and expects the route to return a dict of variables
    that will be made available on the template's context
    """

    def inner(f):
        @wraps(f)
        def decorated(*args, **kwargs):
            response = f(*args, **kwargs)
            if isinstance(response, Response):
                return response
            variables = response if isinstance(response, dict) else {}
            return render_template(template,
                                   is_logged_in=is_logged_in(),
                                   is_admin=is_admin(),
                                   user=session.get('user', None),
                                   **variables)

        return decorated

    return inner
