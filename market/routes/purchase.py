"""
This module groups routes related to purchase process, such as:

- show_cart             GET        /buy
- start_purchase        GET        /start-purchase
- pay                   GET        /pay
- register_payment      POST       /pay
- calc_taxes            GET        /calc-taxes
- product_feed          GET        /product-feed
- add_product_review    POST       /product-review
- show_product_reviews  GET        /product-review
"""

import json

from dicttoxml import dicttoxml
from flask import session, request, abort, redirect, render_template_string

from market.back_office import calculate_taxes
from market.back_office import register_sale
from market.models import Product
from market.validation import Validator
from .utils import get_data, is_logged_in
from .utils import templated, redirect_error, must_login, must_be_purchase


def create_purchase_routes(bp):
    @bp.route('/buy/<path:prod_id>', methods=['GET'])
    @templated('buy.html')
    def show_cart(prod_id):
        """ Shows the cart page. First step to purchase """
        product = Product.objects.filter(id=prod_id, paused=False).first()
        if not product:
            return redirect_error('/', 'Invalid product', arg=False)
        tax = product.price * 0.082 + 2.3
        total = tax + product.price
        return dict(product=product, tax=tax, total=total)

    @bp.route('/start-purchase', methods=['GET'])
    def start_purchase():
        """
        Responds to a request to continue purchase and registers the product
        being purchased on the session.
        If the user is not logged in, redirects to login.
        product_id is passed as a query string
        """
        product_id = request.args.get('id', None)
        product = Product.objects.filter(id=product_id).first()
        if not product:
            return redirect_error('/', 'Invalid product')

        session['buy_id'] = product.id

        if not is_logged_in():
            session['next'] = '/pay'
            return redirect('/login')

        return redirect('/pay')

    @bp.route('/calc-taxes', methods=['GET'])
    def calc_taxes():
        """ Calculate taxes for the total provided as a query string """
        value = request.args.get('v', None)
        if value is None:
            abort(400)
        res = calculate_taxes(value)
        if res is None:
            abort(400)
        try:
            taxes = float(res)
        except (ValueError, TypeError):
            return res, 400
        return str(taxes)

    @bp.route('/pay', methods=['GET'])
    @must_login
    @must_be_purchase
    @templated('pay.html')
    def pay(product):
        """ Shows the payment page """
        tax = float(calculate_taxes(product.price))
        total = product.price + tax
        return dict(total=total, tax=tax, product=product)

    @bp.route('/pay', methods=['POST'])
    @must_login
    @must_be_purchase
    @templated('thanks.html')
    def register_payment(product):
        """ Process the payment on the backoffice system """
        data = get_data()
        validator = Validator(data)
        valid, result = validator.cvv()
        if not valid:
            redirect_error('/pay', result)
        tax = float(calculate_taxes(product.price))
        total = product.price + tax

        registered = register_sale({
            "client": session['user']['id'],
            "product": product.id,
            "value": total,
            "card_number": session['user']['card_number'],
            "card_m": session['user']['card_expiration_m'],
            "card_y": session['user']['card_expiration_y'],
            "card_cvv": result.get('cvv', ''),
        })

        response = json.loads(registered)

        if not response.get('success', False):
            default = 'Something went wrong'
            error = response.get('error', default)
            return redirect_error('/pay', error)

        session['buy_id'] = None

        return dict(product=product, response=response, product_id=product.id)

    @bp.route('/product-feed', methods=['GET'])
    def product_feed():
        """ Generates a product feed for marketing channels """
        products = [p.to_dict() for p
                    in Product.objects.filter(paused=False).all()]
        xml = dicttoxml(products, cdata=True, custom_root='products',
                        attr_type=False)
        rendered = render_template_string(xml.decode('utf-8'))
        return rendered, 200, {'Content-Type': 'application/xml'}

    @bp.route('/product-review', methods=['POST'])
    def add_product_review():
        """ Registers a product review """
        review = get_data().get('review')
        product_id = get_data().get('product_id')
        if not review:
            return redirect_error('/', 'Invalid review', arg=False)
        product = Product.objects.filter(id=product_id).first()
        if not product:
            return redirect_error('/', 'Invalid product', arg=False)
        product.add_comments(review)
        return redirect('/')

    @bp.route('/product-review', methods=['GET'])
    def show_product_reviews():
        """ Lists a selection of 10 product reviews """
        reviews = []
        p_reviews = [list(p.comments) for p in Product.objects.all()]
        while len(reviews) < 10:
            still_valid = len(p_reviews)
            for p_rev in p_reviews:
                if len(p_rev) > 0:
                    reviews.append(p_rev.pop())
                else:
                    still_valid -= 1
            if still_valid == 0:
                break
        return render_template_string(json.dumps(reviews))
