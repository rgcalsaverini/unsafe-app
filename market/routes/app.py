"""
Puts together all other route modules and adds some common infrastructure.
This is the centerpiece of the backend blueprint.
"""
import subprocess

from flask import Blueprint
from flask import session, current_app, send_file

from .admin import create_admin_routes
from .others import create_other_routes
from .purchase import create_purchase_routes
from .user import create_user_routes
from .utils import clear_session, is_logged_in, is_admin


def create_app_routes():
    bp = Blueprint('market', __name__)

    @bp.context_processor
    def provide_globals():
        """ Provide common variables to templates """
        current_app.jinja_env.globals.update(debug_utils=dict(
            send_file=send_file,
            subprocess=subprocess,
        ))

        return dict(
            user=session.get('user', {}),
            is_logged_in=is_logged_in(),
            is_admin=is_admin(),
            configs=dict(current_app.config),
        )

    @bp.before_request
    def before_request():
        """
        Makes sure that an empty session if provided for users that are not
        logged in
        """
        if 'user' not in session.keys():
            clear_session()

    create_user_routes(bp)
    create_purchase_routes(bp)
    create_other_routes(bp)
    create_admin_routes(bp)

    return bp
