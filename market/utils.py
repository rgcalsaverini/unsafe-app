import datetime
import hashlib
import os

from PIL import Image
from flask import Flask
from flask_cors import CORS


def dir_path(file_path):
    return os.path.abspath(os.path.dirname(file_path))


def create_app():
    """ Creates a Flask app """
    app = Flask(__name__)
    app.config['SESSION_COOKIE_HTTPONLY'] = False
    app.config['PERMANENT_SESSION_LIFETIME'] = datetime.timedelta(days=365)
    app.config['SECRET_KEY'] = 'secret'
    app.config['JSON_SORT_KEYS'] = True
    CORS(app)
    return app


def resize_image(path, width):
    """ Return a cached resized image, or change size on the fly  """
    resized_path = '/tmp/{}.{}'.format(width, path.replace('/', '_'))
    if not os.path.isfile(resized_path):
        img = Image.open(path)
        width_pct = (width / float(img.size[0]))
        height = int((float(img.size[1]) * float(width_pct)))
        img = img.resize((width, height), Image.ANTIALIAS)
        img.save(resized_path)

    return resized_path


def md5_of(data):
    """ Return MD5 hash """
    encoded_data = data.encode('utf-8') if isinstance(data, str) else data
    hasher = hashlib.md5()
    hasher.update(encoded_data)
    return hasher.hexdigest()
