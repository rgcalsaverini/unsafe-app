#!/usr/bin/env bash

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi


# Stop nginx and uwsgi
killall uwsgi -I -q -w
service nginx stop

# create log directory
mkdir -p /var/log/market
touch /var/log/market/log
chown www-data.www-data /var/log/market/log

# install requirements
pip3 install -r /market/requirements.txt

# start service
service nginx start
sleep 5
nohup uwsgi --ini /market/uwsgi.ini --lazy-apps&
