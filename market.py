from mongoengine import connect

from market.routes import create_app_routes
from market.utils import create_app

connect('market', host='localhost')

app = create_app()
app.register_blueprint(create_app_routes())

if __name__ == '__main__':
    """ If the app is ran directly, initiate the debug version"""
    app.run(host="0.0.0.0", port=5000, debug=True)
